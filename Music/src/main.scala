import java.text.DateFormat
import java.util.Collection
import java.util.ArrayList
import de.umass.lastfm._
import scala.collection.JavaConversions._
import org.restlet.resource.ClientResource
import org.restlet.data.Form
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import scala.swing._
import scala.swing.Orientation
import scala.swing.event._

object Main extends SimpleSwingApplication {
    // LAST.FM SETUP ///////////////////////////////////////
    var userAgent = "tst"
    val key = "71c24e7398739fd1fe32ef5c0773c68b"										// key given from Last.fm
    Caller.getInstance().setUserAgent(userAgent)
   	//Caller.getInstance().setDebugMode(true) 
    // OTHER SETUP /////////////////////////////////////////
    val format = DateFormat.getDateInstance()
    ////////////////////////////////////////////////////////
    
	def top = new MainFrame {
	    title = "Last.fm Lyrics Finder"
	    preferredSize = new Dimension(700, 900)
	    val userIn = new TextField{text="L_McGuinty"}
	    val inputLabel = new Label {
	    	text = "User Name: "
	    	border = Swing.EmptyBorder(5, 5, 5, 5)
	    }
	    val refreshButton = new Button {
	    	text = "Refresh"//new javax.swing.ImageIcon("c:\\workspace\\gui\\images\\convert.gif")
	    }
	    val lyricsPane = new ScrollPane {
	    	preferredSize = new Dimension(700, 880)
	    	contents = new Label {
			    text = ""
			    border = Swing.EmptyBorder(5, 5, 5, 5)
			    listenTo(refreshButton, userIn)
			    
			    def convert() {
			    	text =  "<html><center><font color = black size = 5>" + 
			    	getCurrentSongLyrics(userIn.text) + "</font></center></html>"
			    }
			    reactions += {
		        	case ButtonClicked(_) => convert()
			    }
		    }
	    }
	    val header = new FlowPanel {
	    	contents.append(inputLabel, userIn, refreshButton)
	    }
	    contents = new BoxPanel(Orientation.Vertical){
	    	contents.append(header, lyricsPane)
	    	border = Swing.EmptyBorder(10, 10, 10, 10)
	    }
  	}    


    
    
    
    
    
    
    
    def getCurrentSongLyrics(user: String): String = {
    	var song=""; var artist=""; var results = List("");
    	results = getCurrentTrack(user)
    	if(results.get(0) == "") return "Failed to retrieve artist and song name from Last.fm"
    	song = results.get(0)
    	artist = results.get(1)
    	return getSongLyrics(song, artist)
    }
    
    def replaceSpace(text: String) : String = {
    	return text.replace(' ', '_')
    }
    
    def printWeeklyArtistChart(user: String) : String = {	      
	    val chart = User.getWeeklyArtistChart(user, 10, key)
	    val from = format.format(chart.getFrom())
	    val to = format.format(chart.getTo())
	    val artists = chart.getEntries()
	    System.out.printf("Charts for %s for the week from %s to %s:%n", user, from, to) 
	    var artistChart = ""
	    for ( artist <- artists ) artistChart += artist.getName().toString()
	    return artistChart
    }
    
    def getCurrentTrack(user: String): List[String] = {
    	try{
    		val recentTracks = User.getRecentTracks(user, key)
    		val tracks = recentTracks.getPageResults()
    		for ( track <- tracks ){
    			if(track.isNowPlaying()) return List(track.getName(), track.getArtist())
    		} 
    	}catch{
    		case e : de.umass.lastfm.CallException => println("Error: Internet not accessible")
    		return List("","")
    	}
        return List("","")
    }
    
    def getSongLyrics(songName: String, artist: String): String = {
	    val form = new Form() 
		//form.add("x", "foo") 
		//form.add("y", "bar") 
		
		val wantedSong = replaceSpace(songName)
		val wantedArtist = replaceSpace(artist)
		val resource = new ClientResource("http://lyrics.wikia.com/api.php?func=getSong&artist="
				+wantedArtist+"&song="+wantedSong);
		val response = resource.put(form.getWebRepresentation()) //.post(form.getWebRepresentation()) 
		if (response.isAvailable()) {
	    	try{
	    		val url = Jsoup.parse(response.getText()).select("a[href]").head.attributes().head.getValue()
			    val doc = Jsoup.connect(url).timeout(0).get()
			    val lyricsAndBS = doc.getElementsByAttributeValue("class", "lyricbox").head
			    //3 and 14 are somewhat magic numbers I'm afraid
			    val lyricsAsArray = lyricsAndBS.toString().split('\n').drop(3).dropRight(14)
			    var lyrics = "<font color = black size = 7>"+artist + "</font><br><font color = black size = 6>" +songName + "<br></font></center>" 
	    		for(x <- lyricsAsArray){
	    			lyrics += x.toString()
	    		}
			    return lyrics
		    }
		    catch{
			    case e : Throwable => return "Failed to get lyrics from http://lyrics.wikia.com <br>\n" +
	    	  		"This song is not supported <br>" +
	    	  		"You're probably too indie or spelling the song name wrong <br>" +
	    	  		"Ya'know, because it's obviously not my fault..."
	    	}
		}
		return "Lyrics could not be found due to failed rest call, please check internet connecion"
    } 
}